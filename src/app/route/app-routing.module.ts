import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../service/auth-guard.service'

import { Calculator } from '../pages/calc/calc.component'
import { Area } from '../pages/area/area.component'
import { Login } from '../pages/login/login.component';
import { Home } from '../pages/home/home.component';

const routes: Routes = [
  { path: 'home', component: Home, canActivate: [AuthGuard] },
  { path: 'calc', component: Calculator, canActivate: [AuthGuard] },
  { path: 'area', component: Area },
  { path: 'login', component: Login },
  { path: '', component: Login }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
