import { Component } from '@angular/core';
import { AuthenticationService } from '../../service/authentication.service'
import { Router } from '@angular/router'

@Component({
  selector: 'login',
  templateUrl: './login.component.html'
})
export class Login {
  username: string;
  password: string;
  constructor(private _router: Router, private _auth: AuthenticationService) {
    if (this._auth.loggedIn) {
      this._router.navigate(['home'])
    }
  }

  login(): void {
    if (this.username != "" && this.password != "") {
      if (this._auth.login(this.username, this.password)) {
        this._router.navigate(['home'])
      }
      else
        alert("Username atau password anda salah")
    }
  }
}
