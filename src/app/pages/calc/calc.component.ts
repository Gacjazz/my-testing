import { Component } from '@angular/core';

@Component({
  selector: 'calc',
  templateUrl: './calc.component.html',
  styleUrls: ['./calc.component.css']
})
export class Calculator {
  public number1: number;
  public number2: number;
  public result: number;
  public status: boolean = true

  public add() {
    this.result = this.number1 + this.number2 
  }

  public minus() {
    this.result = this.number1 - this.number2
  }

  public toggle() {
    this.status = false;
  }

}
