import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from "./route/app-routing.module"
import { AppBootstrapModule } from './app-bootstrap.module'
import { AuthenticationService } from './service/authentication.service'

// page
import { AppComponent } from './pages/app/app.component'
import { AppHeaderComponent } from './component/appheader/appheader.component'
import { Calculator } from './pages/calc/calc.component'
import { Area } from './pages/area/area.component';
import { Home } from './pages/home/home.component'
import { Login } from './pages/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    Calculator,
    Area,
    Home,
    Login
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    AppBootstrapModule
  ],
  providers: [AuthenticationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
